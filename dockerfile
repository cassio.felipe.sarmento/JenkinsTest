FROM maven:3-openjdk-17 as build
WORKDIR /myapp
COPY . .
RUN mvn clean && mvn package
RUN mkdir build
WORKDIR /myapp/target
RUN cp *.war /myapp/build/app.war

FROM tomcat:11.0-jdk17
COPY --from=build /myapp/build ./webapps
EXPOSE 8080

