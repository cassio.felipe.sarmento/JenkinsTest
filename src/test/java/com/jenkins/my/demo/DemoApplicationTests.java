package com.jenkins.my.demo;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.jenkins.my.demo.controllers.IndexController;


@SpringBootTest
class DemoApplicationTests {

	@Autowired
	IndexController indexController;

	@Test
	void contextLoads() {
		Assertions.assertThat(indexController).isNotNull();
	}

}
